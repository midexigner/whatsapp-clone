Front end

firebase
   authenication
   hosting

Backend
    express
    nodejs
    mongodb

- git init
- npm init

-- open `package.json`

```sh
"type": "module",
"scripts": {
    "start":"node server.js",
    "test": "echo \"Error: no test specified\" && exit 1"
  },
```
- npm i express mongoose
- create `.gitignore`
- npm i -g nodemon

- `app.js`

```sh
//  importing
import express from 'express';
import mongoose from 'mongoose'

// app config
const app = express();
const port = process.env.PORT || 9000

// middleware

// DB config
const connection_url = 'mongodb+srv://admin:Devs@321$@cluster0.zwzfq.mongodb.net/whatsappdb?retryWrites=true&w=majority'
mongoose.connect('connection_url',{
    useCreateIndex:true,
    useNewUrlParser:true,
    useUnifiedTopology:true
})

// ????

// api routes
app.get('/',(req,res)=> res.status(200).send("Hello World"))

//  listen
app.listen(port,()=>console.log(`Listen on localhost:${port}`))
```
- npm install pusher

- `model.Message.js`

```sh
import mongoose from "mongoose";

const whatsappSchema = mongoose.Schema({
  message: String,
  name: String,
  timestamp: String,
  received: Boolean,
});

//collection
export default mongoose.model("messagecontents", whatsappSchema);
```

- npm i cors
-- React js
- npm i pusher-js