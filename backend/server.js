//  importing
import express from 'express';
import mongoose from 'mongoose'
import Messages from './dbMessages.js';
import Pusher from "pusher";
import cors from "cors";


// app config
const app = express();
const port = process.env.PORT || 9000

const pusher = new Pusher({
    appId: '1092861',
    key: 'c75e6cf55aaf0361d62e',
    secret: 'c869a501aad5edd2d25b',
    cluster: 'eu',
    encrypted: true
  });

// middleware
app.use(express.json());
app.use(cors());
/* app.use((req,res,next)=>{
    res.setHeader("Access-Control-Allow-Origin","*");
    res.setHeader("Access-Control-Allow-Headers","*");
    next();
}) */



// DB config
const connection_url = "mongodb+srv://admin:Devs@321$@cluster0.zwzfq.mongodb.net/whatsappdb?retryWrites=true&w=majority";

mongoose.connect(connection_url,{
    useCreateIndex:true,
    useNewUrlParser:true,
    useUnifiedTopology:true
})

// pusher
const db = mongoose.connection;

db.once("open",()=>{
    console.log("DB connected");
    const msgCollection = db.collection("messagecontents");
  const changeStream = msgCollection.watch();
//   console.log(changeStream);
  changeStream.on("change", (change) => {
    console.log("A change occured", change);

    if (change.operationType === "insert") {
      const messageDetails = change.fullDocument;
      pusher.trigger("message", "inserted", {
        name: messageDetails.name,
        message: messageDetails.message,
        timestamp: messageDetails.timestamp,
        received: messageDetails.received,
      });
    } else {
      console.log("Error triggering Pusher");
    }
  });
});


// api routes
app.get('/',(req,res)=> res.status(200).send("Hello World"));

app.get("/api/v1/messages/sync", (req, res) => {
    Messages.find((err, data) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.status(200).send(data);
      }
    });
  });
app.post("/api/v1/messages/new", (req, res) => {
    const dbMessage = req.body;
    Messages.create(dbMessage, (err, data) => {
      if (err) {
        res.status(500).send(err);
      } else {
        res.status(201).send(data);
      }
    });
  });


//  listen
app.listen(port,()=>console.log(`Listen on localhost:${port}`))