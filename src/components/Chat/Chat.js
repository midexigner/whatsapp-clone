import React, { useState } from 'react'
import './Chat.css'
import { Avatar, IconButton } from '@material-ui/core'
import { SearchOutlined,AttachFile,MoreVert} from '@material-ui/icons';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon'
import MicIcon from '@material-ui/icons/Mic'
import axios from '../../axios'


function Chat({ messages }) {
    const [input, setInput] = useState("");
    
    const sendMessage = async (e) => {
        e.preventDefault();
    
        await axios.post("/api/v1/messages/new", {
          message: input,
          name: "MI",
          timestamp: "Just now",
          received: true,
        });
    
        setInput("");
      };

    return (
        <div className="chat">
            <div className="chat__header">
                <Avatar/>
                <div className="chat__headerInfo">
                    <h3>Room name</h3>
                    <p>Last Seen at</p>
                </div>
                <div className="chat__header_right">
                
                    <IconButton>
                        <SearchOutlined/>
                    </IconButton>
                    <IconButton>
                        <AttachFile/>
                    </IconButton>
                    <IconButton>
                        <MoreVert/>
                    </IconButton>
                </div>
            </div>
            <div className="chat__body">
            { messages.map((message, i) => {
          return (
                <p key={i} className={`chat__message ${
                    message.received && "chat__reciver"
                  }`}>
                    <span className="chat__name">{message.name}</span>
                    {message.message}
                    <span className="chat__timestamp">
                    {message.timestamp}
                    </span>
                </p>
            )
            })}
               
                
            </div>
            <div className="chat__footer">
                <InsertEmoticonIcon />
                <form>
          <input
            type="text"
            placeholder="Type a message"
            onChange={(e) => setInput(e.target.value)}
          />
          <button type="submit" onClick={sendMessage}>
            Send a message
          </button>
        </form>
        <MicIcon />
            </div>
        </div>
    )
}

export default Chat
