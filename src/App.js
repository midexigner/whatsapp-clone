import React, { useEffect, useState } from 'react';
import './App.css';
import Chat from './components/Chat/Chat';
import Sidebar from './components/Sidebar/Sidebar';

import Pusher from "pusher-js";
import axios from "./axios";


function App() {
  const [messages, setMessages] = useState([]);
  useEffect(() => {
   axios.get("/api/v1/messages/sync").then((response) => {
      setMessages(response.data);
    });
  }, []);
  useEffect(()=>{

    const pusher = new Pusher("c75e6cf55aaf0361d62e", {
      cluster: "eu",
    });

    const channel = pusher.subscribe("message");
    channel.bind("inserted", (newMessage) => {
      alert(JSON.stringify(newMessage))
      setMessages([...messages, newMessage]);
    });

return ()=>{
  channel.unbind_all();
  channel.unsubscribe();
}

  },[messages])

  return (
    <div className="app">
     <div className="app__body">
     <Sidebar/>
     <Chat messages={messages} />
     </div>
     
    </div>
  );
}

export default App;
